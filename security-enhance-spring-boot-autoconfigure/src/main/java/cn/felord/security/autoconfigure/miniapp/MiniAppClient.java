package cn.felord.security.autoconfigure.miniapp;

import lombok.Data;

/**
 * @author felord.cn
 * @since 1.0.8.RELEASE
 */
@Data
public class MiniAppClient {
    private String clientId;
    private String appId;
    private String secret;
}
