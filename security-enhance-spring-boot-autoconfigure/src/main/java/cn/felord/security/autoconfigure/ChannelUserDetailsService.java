package cn.felord.security.autoconfigure;

import cn.felord.security.autoconfigure.miniapp.MiniAppRequest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * The interface Channel user details service.
 *
 * @author felord.cn
 * @since 1.0.8.RELEASE
 */
public interface ChannelUserDetailsService extends UserDetailsService {
    /**
     * 验证码登录
     *
     * @param phone the phone
     * @return the user details
     */
    UserDetails loadByPhone(String phone);
    /**
     * 小程序在微信登录成功后发起后端登录的方法
     *
     * @param request the request
     * @return the user details
     */
    UserDetails miniAppLogin(MiniAppRequest request);

}
