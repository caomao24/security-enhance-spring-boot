package cn.felord.security.autoconfigure.miniapp;

import lombok.Data;

/**
 * @author n1
 * @since 2021/6/25 11:19
 */
@Data
public class MiniAppRequest {
    private String clientId;
    private String openId;
    private String unionId;
    private String iv;
    private String encryptedData;
}
