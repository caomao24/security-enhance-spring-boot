package cn.felord.security.autoconfigure;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;

/**
 * The interface Http security customizer.
 *
 * @author felord.cn
 * @since 2021 /4/27 11:04
 */
@FunctionalInterface
public interface HttpSecurityCustomizer {

    /**
     * Customize.
     *
     * @param http the http
     */
    void customize(HttpSecurity http);
}
