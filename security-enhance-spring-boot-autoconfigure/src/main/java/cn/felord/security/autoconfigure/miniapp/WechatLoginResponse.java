package cn.felord.security.autoconfigure.miniapp;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * @author n1
 * @since 2021/6/23 15:40
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class WechatLoginResponse {
    private Integer errcode;
    private String errmsg;
    @JsonAlias({"session_key"})
    private String sessionKey;
    private String openid;
    private String unionid;
}
