package cn.felord.security.autoconfigure;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author felord.cn
 * @since 2021/4/6 13:59
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({ServletWebSecurityConfiguration.class,ChannelAuthenticationConfiguration.class,JwtConfiguration.class})
public @interface EnableSpringSecurity {
}
