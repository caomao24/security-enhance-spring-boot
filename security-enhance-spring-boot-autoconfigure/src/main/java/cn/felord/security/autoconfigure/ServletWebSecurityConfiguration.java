package cn.felord.security.autoconfigure;


import cn.felord.security.autoconfigure.handler.LoginAuthenticationSuccessHandler;
import cn.felord.security.autoconfigure.handler.SimpleAccessDeniedHandler;
import cn.felord.security.autoconfigure.handler.SimpleAuthenticationEntryPoint;
import cn.felord.security.autoconfigure.jwt.JwtTokenGenerator;
import cn.felord.security.autoconfigure.miniapp.MiniAppClientService;
import cn.felord.security.autoconfigure.miniapp.MiniAppPreAuthenticationFilter;
import cn.felord.security.autoconfigure.miniapp.MiniAppSessionKeyCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.AuthenticationEntryPointFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;


/**
 * The Servlet web security configuration.
 *
 * @author felord.cn
 * @since 2021 /3/26 11:35
 */
@Configuration(proxyBeanMethods = false)
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
public class ServletWebSecurityConfiguration {
    private List<HttpSecurityCustomizer> httpSecurityCustomizers = Collections.emptyList();

    /**
     * Role checker role checker.
     *
     * @param function the function
     * @return the role checker
     */
    @Bean
    @ConditionalOnMissingBean
    RoleChecker roleChecker(Function<Set<String>, Set<AntPathRequestMatcher>> function) {
        return new JdbcRoleChecker(function);
    }
    /**
     * Mini app pre authentication filter mini app pre authentication filter.
     *
     * @param miniAppClientService   the mini app client service
     * @param miniAppSessionKeyCache the mini app session key cache
     * @return the mini app pre authentication filter
     */
    @Bean
    @ConditionalOnBean({MiniAppClientService.class, MiniAppSessionKeyCache.class})
    MiniAppPreAuthenticationFilter miniAppPreAuthenticationFilter(MiniAppClientService miniAppClientService,
                                                                  MiniAppSessionKeyCache miniAppSessionKeyCache) {
        return new MiniAppPreAuthenticationFilter(miniAppClientService, miniAppSessionKeyCache);
    }
    /**
     * Web security filter chain security filter chain.
     *
     * @param httpSecurity                          the http security
     * @param jwtTokenGenerator                     the jwt token generator
     * @param bearerAccessTokenAuthenticationFilter the bearer access token authentication filter
     * @return the security filter chain
     * @throws Exception the exception
     */
    @Bean
    @ConditionalOnMissingBean(name = "defaultWebSecurityFilterChain")
    SecurityFilterChain defaultWebSecurityFilterChain(HttpSecurity httpSecurity, JwtTokenGenerator jwtTokenGenerator, BearerAccessTokenAuthenticationFilter bearerAccessTokenAuthenticationFilter) throws Exception {

        SimpleAuthenticationEntryPoint authenticationEntryPoint = new SimpleAuthenticationEntryPoint();
        httpSecurity.csrf().disable()
                .cors()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .exceptionHandling()
                .accessDeniedHandler(new SimpleAccessDeniedHandler())
                .authenticationEntryPoint(authenticationEntryPoint)
                .and()
                .authorizeRequests().anyRequest().access("@roleChecker.check(authentication,request)")
                .and()
                .addFilterBefore(bearerAccessTokenAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
                .formLogin()
                .successHandler(new LoginAuthenticationSuccessHandler(jwtTokenGenerator))
                .failureHandler(new AuthenticationEntryPointFailureHandler(authenticationEntryPoint));

        ApplicationContext applicationContext = httpSecurity.getSharedObject(ApplicationContext.class);

        MiniAppPreAuthenticationFilter miniAppPreAuthenticationFilter = getBeanOrNull(applicationContext, MiniAppPreAuthenticationFilter.class);
        if (Objects.nonNull(miniAppPreAuthenticationFilter)) {
            httpSecurity.addFilterBefore(miniAppPreAuthenticationFilter, BearerAccessTokenAuthenticationFilter.class);
        }
        ChannelAuthenticationFilter channelAuthenticationFilter = getBeanOrNull(applicationContext, ChannelAuthenticationFilter.class);
        if (Objects.nonNull(channelAuthenticationFilter)) {
            httpSecurity.addFilterBefore(channelAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);
        }
        httpSecurityCustomizers
                .forEach(httpSecurityCustomizer -> httpSecurityCustomizer.customize(httpSecurity));
        return httpSecurity.build();
    }

    @Autowired(required = false)
    void setWebSecurityCustomizers(List<HttpSecurityCustomizer> httpSecurityCustomizers) {
        this.httpSecurityCustomizers = httpSecurityCustomizers;
    }

    /**
     * Swagger web security customizer web security customizer.
     *
     * @return the web security customizer
     */
    @Bean
    WebSecurityCustomizer swaggerWebSecurityCustomizer() {
        return web -> {
            //所需要用到的静态资源，允许访问
            web.ignoring().antMatchers("/swagger-ui.html",
                    "/swagger-ui/*",
                    "/error",
                    "/swagger-resources/**",
                    "/v2/api-docs",
                    "/v3/api-docs",
                    "/webjars/**");

        };
    }

    /**
     * Cors configuration source cors configuration source.
     *
     * @return the cors configuration source
     */
    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Collections.singletonList(CorsConfiguration.ALL));
        configuration.setAllowedMethods(Collections.singletonList(CorsConfiguration.ALL));
        configuration.setAllowedHeaders(Collections.singletonList(CorsConfiguration.ALL));
        configuration.setExposedHeaders(Collections.singletonList(CorsConfiguration.ALL));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    private <T> T getBeanOrNull(ApplicationContext applicationContext, Class<T> beanType) {

        String[] beanNames = applicationContext
                .getBeanNamesForType(beanType);
        if (beanNames.length != 1) {
            return null;
        }
        return applicationContext.getBean(beanNames[0], beanType);
    }

}
