package cn.felord.security.autoconfigure;


import cn.felord.security.autoconfigure.handler.LoginAuthenticationSuccessHandler;
import cn.felord.security.autoconfigure.handler.SimpleAuthenticationEntryPoint;
import cn.felord.security.autoconfigure.jwt.JwtTokenGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.web.authentication.AuthenticationEntryPointFailureHandler;

import java.util.List;

/**
 * The type Captcha security configuration.
 *
 * @author felord.cn
 * @since 1.0.8.RELEASE
 */
@Configuration(proxyBeanMethods = false)
public class ChannelAuthenticationConfiguration {
    /**
     * Channel authentication filter channel authentication filter.
     *
     * @param channelFilters the channel filters
     * @return the channel authentication filter
     */
    @Bean
    public ChannelAuthenticationFilter channelAuthenticationFilter(List<LoginChannelProcessor<? extends AbstractAuthenticationToken>> channelFilters, JwtTokenGenerator jwtTokenGenerator) {
        ChannelAuthenticationFilter channelAuthenticationFilter = new ChannelAuthenticationFilter(channelFilters);
        channelAuthenticationFilter.setAuthenticationSuccessHandler(new LoginAuthenticationSuccessHandler(jwtTokenGenerator));
        SimpleAuthenticationEntryPoint authenticationEntryPoint = new SimpleAuthenticationEntryPoint();
        channelAuthenticationFilter.setAuthenticationFailureHandler(new AuthenticationEntryPointFailureHandler(authenticationEntryPoint));

        return channelAuthenticationFilter;
    }
}
