package cn.felord.security.autoconfigure;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.AuthenticationProvider;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * The interface Login channel processor.
 *
 * @param <T> the type parameter
 * @author n1
 * @since 2021 /6/29 9:28
 */
public interface LoginChannelProcessor<T extends AbstractAuthenticationToken> {

    /**
     * Authentication request authentication.
     *
     * @param request the request
     * @return the authentication
     * @throws IOException the io exception
     */
    T authenticationRequest(HttpServletRequest request) throws IOException;

    /**
     * Gets provider.
     *
     * @return the provider
     */
    AuthenticationProvider getProvider();

    /**
     * Supports boolean.
     *
     * @param channel the channel
     * @return the boolean
     */
    boolean supports(String channel);
}
