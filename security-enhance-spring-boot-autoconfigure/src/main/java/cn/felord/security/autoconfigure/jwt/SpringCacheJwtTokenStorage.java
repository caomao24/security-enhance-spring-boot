package cn.felord.security.autoconfigure.jwt;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.oauth2.core.endpoint.OAuth2AccessTokenResponse;

/**
 * @author felord.cn
 * @since 2021/3/27 12:54
 */
public class SpringCacheJwtTokenStorage implements JwtTokenStorage {
    private static final String TOKEN_CACHE = "usrTkn";


    @CachePut(value = TOKEN_CACHE, key = "#userId")
    @Override
    public BearerTokenResponse put(OAuth2AccessTokenResponse tokenResponse, String userId) {
        return BearerTokenResponse.from(tokenResponse);
    }
    @CacheEvict(value = TOKEN_CACHE, key = "#userId")
    @Override
    public void expire(String userId) {

    }
    @Cacheable(value = TOKEN_CACHE, key = "#userId")
    @Override
    public BearerTokenResponse get(String userId) {
        return null;
    }
}
